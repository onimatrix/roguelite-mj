class SpriteSheet
{
	PImage[] frames;

	SpriteSheet(String ssheetPath, int frameW, int frameH, int paddingX, int paddingY)
	{
		PImage ssheet = loadImage(ssheetPath);
		int framesInX = ssheet.width / (frameW + paddingX);
		int framesInY = ssheet.height / (frameH + paddingY);

		this.frames = new PImage[framesInX * framesInY];
		for (int y = 0; y < framesInY; y++)
		{
			for (int x = 0; x < framesInX; x++)
			{
				PImage frame = createImage(frameW, frameH, ARGB);
				frame.copy(ssheet, x * (frameW + paddingX), y * (frameH + paddingY), frameW, frameH, 0, 0, frameW, frameH);
				this.frames[x + y * framesInX] = frame;
			}
		}
	}
};
