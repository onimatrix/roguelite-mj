class AnimationManager
{
	HashMap<String, Animation> animations;
	Animation activeAnimation;
	String activeAnimationID;

	AnimationManager()
	{
		this.animations = new HashMap<String, Animation>();
		this.activeAnimation = null;
		this.activeAnimationID = null;
	}

	void addFrame(String animID, PImage frame)
	{
		Animation anim = this.animations.get(animID);
		if (anim == null)
		{
			anim = new Animation();
			this.animations.put(animID, anim);
		}
		anim.addFrame(frame);
	}

	void setActive(String animID)
	{
		if (this.activeAnimationID != null && this.activeAnimationID.equals(animID))
			return;

		this.activeAnimation = this.animations.get(animID);
		this.activeAnimationID = animID;
		this.activeAnimation.reset();
	}

	void draw(float dt)
	{
		this.activeAnimation.draw(dt);
	}
};

class Animation
{
	final static float FRAMES_PER_SECOND = 6.0f;
	final static float FRAME_DURATION = 1.0f / FRAMES_PER_SECOND;

	ArrayList<PImage> frames;
	int currentFrame;
	float frameTime;

	Animation()
	{
		this.frames = new ArrayList<PImage>();
		reset();
	}

	void reset()
	{
		this.currentFrame = 0;
		this.frameTime = 0;
	}

	void addFrame(PImage frame)
	{
		this.frames.add(frame);
	}

	void draw(float dt)
	{
		if (this.frames.size() == 0)
			return;

		image(this.frames.get(this.currentFrame), 0, 0);
		this.frameTime += dt;

		while (this.frameTime > FRAME_DURATION)
		{
			this.frameTime -= FRAME_DURATION;
			this.currentFrame = (this.currentFrame + 1) % this.frames.size();
		}
	}
};
