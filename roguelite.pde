UIRoot ui;

Dungeon dungeon;
Room currentRoom;
Player player;
Enemy enemy;

int lastDraw;

int winWidth;
int winHeight;

void setup()
{
	size(1024, 768);
	winWidth = width;
	winHeight = height;

	noSmooth();
	surface.setResizable(true);

	textFont(createFont("Comic Sans Bold", 24));

	ui = new UIRoot();

	UIPopup popup = ui.attach(new UIPopup(), C, C);
	popup.withW(Coord.REL, 50);
	popup.withH(Coord.REL, 50);

	UIObject center = popup.container.attach(new UIImage(), C, C)
		.withPath("test.png");
	UIObject left = center.attach(new UIImage(), L, R)
		.withPath("test.png");
	center.attach(new UIImage(), R, L)
		.withPath("test.png");
/*
	center.attach(new UIMoveable(), C, C)
		.moves(center);
*/
	UIPopup popup2 = ui.attach(new UIPopup(), C, C);
	popup2.withTitle("Chiquito");
	popup2.withW(Coord.REL, 50);
	popup2.withH(Coord.REL, 50);

	center.attach(new UIReparenteable())
		.reparents(center);
	popup.container.attach(new UIDragNDropTarget());
	popup2.container.attach(new UIDragNDropTarget());


	ui.layout();

	dungeon = new Dungeon(8, 8, 8, 14);
	currentRoom = dungeon.startRoom();
	currentRoom.tiles[4][1].setObstacle(true);
	currentRoom.tiles[4][2].setObstacle(true);
	currentRoom.tiles[4][3].setObstacle(true);
	currentRoom.tiles[4][4].setObstacle(true);
	currentRoom.tiles[5][4].setObstacle(true);
	currentRoom.tiles[6][4].setObstacle(true);

	currentRoom.tiles[6][6].setObstacle(true);
	currentRoom.tiles[6][7].setObstacle(true);
	currentRoom.tiles[7][7].setObstacle(true);
	currentRoom.tiles[8][7].setObstacle(true);
	currentRoom.tiles[9][7].setObstacle(true);
	currentRoom.tiles[9][8].setObstacle(true);
	currentRoom.tiles[9][9].setObstacle(true);
	currentRoom.createDMap();

	player = new Player();
	enemy = new Enemy(new PVector(width * 0.75f, height * 0.5f));

	lastDraw = millis();
}

void draw()
{
	if (winWidth != width || winHeight != height)
	{
		ui.layout();
		winWidth = width;
		winHeight = height;
	}

	int now = millis();
	int frameTime = now - lastDraw;
	lastDraw = now;
	float dt = frameTime / 1000.0f;

	player.update();
	enemy.update(dt);

	background(255, 0, 255);
	currentRoom.draw();

	enemy.draw(dt);
	player.draw(dt);

	dungeon.drawMap(0, 0, 160, 120);

	ui.visit();
}

boolean kLEFT = false;
boolean kRIGHT = false;
boolean kUP = false;
boolean kDOWN = false;

void keyPressed()
{
	if (key == 'a') kLEFT = true;
	if (key == 'd') kRIGHT = true;
	if (key == 'w') kUP = true;
	if (key == 's') kDOWN = true;
}

void keyReleased()
{
	if (key == 'a') kLEFT = false;
	if (key == 'd') kRIGHT = false;
	if (key == 'w') kUP = false;
	if (key == 's') kDOWN = false;

	if (key == ' ') dungeon.regenerate(8, 14);
}

boolean mouseLeft = false;
void mousePressed()
{
	if (mouseButton == LEFT)
	{
		if (mouseLeft == false)
			ui.broadcast(new UIEvent(UIEvent.MOUSE_PRESSED));
		mouseLeft = true;
	}
}

void mouseReleased()
{
	if (mouseButton == LEFT)
	{
		if (mouseLeft)
			ui.broadcast(new UIEvent(UIEvent.MOUSE_RELEASE));
		mouseLeft = false;
	}
}
