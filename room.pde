class Tile
{
	int x;
	int y;

	color col;
	boolean obstacle;

	Tile(int x, int y)
	{
		this.x = x;
		this.y = y;

		if (this.x == 0 || this.x == Room.TILES_ON_X - 1 ||
			this.y == 0 || this.y == Room.TILES_ON_Y - 1)
		{
			setObstacle(true);
		}
		else
			setObstacle(false);
	}

	void setObstacle(boolean state)
	{
		this.obstacle = state;
		if (this.obstacle)
			this.col = color(128);
		else
		{
			if ((this.x + this.y) % 2 == 0)
				this.col = color(32);
			else
				this.col = color(40);
		}
	}
};

class RoomStart extends Room
{
};

class RoomEnd extends Room
{
};

class Room
{
	static final int TILES_ON_X = 16;
	static final int TILES_ON_Y = 12;

	int x;
	int y;
	Tile[][] tiles;
	DMap dmap;

	boolean doorLeft;
	boolean doorRight;
	boolean doorTop;
	boolean doorBottom;

	Room()
	{
		this.tiles = new Tile[Room.TILES_ON_X][Room.TILES_ON_Y];
		for (int y = 0; y < Room.TILES_ON_Y; y++)
			for (int x = 0; x < Room.TILES_ON_X; x++)
				this.tiles[x][y] = new Tile(x, y);
	}

	void createDMap()
	{
		this.dmap = new DMap(Room.TILES_ON_X, Room.TILES_ON_Y);
		for (int y = 0; y < Room.TILES_ON_Y; y++)
			for (int x = 0; x < Room.TILES_ON_X; x++)
			{
				Tile t = this.tiles[x][y];
				if (t != null && t.obstacle)
					this.dmap.setObstacle(x, y);
			}
	}

	void openDoors()
	{
		if (this.doorLeft)
		{
			this.tiles[0][Room.TILES_ON_Y / 2 - 1].setObstacle(false);
			this.tiles[0][Room.TILES_ON_Y / 2].setObstacle(false);
		}
		if (this.doorRight)
		{
			this.tiles[Room.TILES_ON_X - 1][Room.TILES_ON_Y / 2 - 1].setObstacle(false);
			this.tiles[Room.TILES_ON_X - 1][Room.TILES_ON_Y / 2].setObstacle(false);
		}
		if (this.doorTop)
		{
			this.tiles[Room.TILES_ON_X / 2 - 1][0].setObstacle(false);
			this.tiles[Room.TILES_ON_X / 2][0].setObstacle(false);
		}
		if (this.doorBottom)
		{
			this.tiles[Room.TILES_ON_X / 2 - 1][Room.TILES_ON_Y - 1].setObstacle(false);
			this.tiles[Room.TILES_ON_X / 2][Room.TILES_ON_Y - 1].setObstacle(false);
		}
	}

	int getTileX(float x)
	{
		float tileW = width / float(Room.TILES_ON_X);
		int tx = int(floor(x / tileW));
		if (tx < 0)
			tx = 0;
		else if (tx >= Room.TILES_ON_X)
			tx = Room.TILES_ON_X - 1;
		return tx;
	}

	int getTileY(float y)
	{
		float tileH = height / float(Room.TILES_ON_Y);
		int ty = int(floor(y / tileH));
		if (ty < 0)
			ty = 0;
		else if (ty >= Room.TILES_ON_Y)
			ty = Room.TILES_ON_Y - 1;
		return ty;
	}

	ArrayList<Tile> getOverlappingTiles(RoomObject robj)
	{
		ArrayList<Tile> overlapping = new ArrayList<Tile>();

		float radius = width / robj.getRadius();

		float left = player.pos.x - radius * 0.5f;
		float right = player.pos.x + radius * 0.5f;
		float top = player.pos.y - (radius * 0.25f) * 0.5f;
		float bottom = player.pos.y + (radius * 0.25f) * 0.5f;

		int leftTile = getTileX(left);
		int rightTile = getTileX(right);
		int topTile = getTileY(top);
		int bottomTile = getTileY(bottom);

		for (int y = topTile; y <= bottomTile; y++)
			for (int x = leftTile; x <= rightTile; x++)
				overlapping.add(this.tiles[x][y]);

		return overlapping;
	}

	void draw()
	{
		float tileW = width / float(Room.TILES_ON_X);
		float tileH = height / float(Room.TILES_ON_Y);

		noStroke();
		for (int y = 0; y < Room.TILES_ON_Y; y++)
			for (int x = 0; x < Room.TILES_ON_X; x++)
			{
				fill(this.tiles[x][y].col);
				rect(x * tileW, y * tileH, tileW, tileH);
			}
	}
};
