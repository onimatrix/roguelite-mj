class UIButton extends UIObject
{
	UIButton withBackground(color col)
	{
		attach(new UIRect())
			.withColor(col);
		return this;
	}

	UIButton withCallback(Object target, String functionName)
	{
		attach(new UIClickeable(target, functionName));
		return this;
	}

	UIButton withCallback(Object target, String functionName, Object userData)
	{
		attach(new UIClickeable(target, functionName, userData));
		return this;
	}
};

class UIFollowMouse extends UIObject
{
	UIObject target;

	UIFollowMouse moves(UIObject target)
	{
		this.target = target;
		return this;
	}

	void draw()
	{
		int dx = mouseX - pmouseX;
		int dy = mouseY - pmouseY;

		if (this.target != null)
		{
			if (this.target.ox.type == Coord.PX)
				this.target.ox.value += dx;
			else if (this.target.ox.type == Coord.REL)
				this.target.ox.value += dx * 100.0f / float(this.target.parent.w);

			if (this.target.oy.type == Coord.PX)
				this.target.oy.value += dy;
			else if (this.target.oy.type == Coord.REL)
				this.target.oy.value += dy * 100.0f / float(this.target.parent.h);

			this.target.layout();
		}
	}
};

class UIMoveable extends UIObject
{
	UIObject target;
	boolean dragged;

	UIMoveable()
	{
		attach(new UIDraggeable(this, "cb_dragStart", this, "cb_dragEnd"));
	}

	UIMoveable moves(UIObject target)
	{
		this.target = target;
		return this;
	}

	void cb_dragStart()
	{
		this.dragged = true;
	}

	void cb_dragEnd()
	{
		this.dragged = false;
	}

	void draw()
	{
		if (this.dragged)
		{
			int dx = mouseX - pmouseX;
			int dy = mouseY - pmouseY;

			if (this.target != null)
			{
				if (this.target.ox.type == Coord.PX)
					this.target.ox.value += dx;
				else if (this.target.ox.type == Coord.REL)
					this.target.ox.value += dx * 100.0f / float(this.target.parent.w);

				if (this.target.oy.type == Coord.PX)
					this.target.oy.value += dy;
				else if (this.target.oy.type == Coord.REL)
					this.target.oy.value += dy * 100.0f / float(this.target.parent.h);

				this.target.layout();
			}
		}
	}
};

class UIReparenteable extends UIObject
{
	UIObject target;

	UIObject followMouse;
	UIObject targetParent;
	Coord targetX;
	Coord targetY;
	int offsetX;
	int offsetY;

	UIReparenteable()
	{
		attach(new UIDragNDroppeable(this, "drag", this, "drop"));
	}

	UIReparenteable reparents(UIObject target)
	{
		this.target = target;
		return this;
	}

	void drag(Object draggedObj)
	{
		this.targetParent = this.target.parent;
		this.targetX = this.target.ox.copy();
		this.targetY = this.target.oy.copy();

		UIRoot root = getRoot();
		this.target.dettach();
		root.attach(this.target);

		this.offsetX = mouseX - this.target.x;
		this.offsetY = mouseY - this.target.y;
		this.target.ox = new Coord(Coord.PX, this.target.x, true);
		this.target.oy = new Coord(Coord.PX, this.target.y, false);

		this.followMouse = this.target.attach(new UIFollowMouse().moves(this.target));
	}

	void drop(Object droppedOnObj)
	{
		this.followMouse.dettach();
		if (droppedOnObj instanceof UIDragNDropTarget == false)
		{
			this.target.dettach();
			this.targetParent.attach(this.target);
			this.target.ox = this.targetX;
			this.target.oy = this.targetY;
		}
		else
		{
			UIObject newParent = (UIObject)droppedOnObj;
			this.target.dettach();
			newParent.attach(this.target)
				.withX(Coord.PX, mouseX - (newParent.x + this.offsetX))
				.withY(Coord.PX, mouseY - (newParent.y + this.offsetY));
		}
		this.target.layout();

		this.targetParent = null;
		this.targetX = null;
		this.targetY = null;
	}
};

class UIPopup extends UIObject
{
	UIText title;
	UIObject container;

	UIPopup()
	{
		UIObject closeButton = attach(new UIButton(), RT, RT)
			.withBackground(color(255, 0, 0))
			.withCallback(this, "cb_close")
			.withW(Coord.PX, 32)
			.withH(Coord.PX, 32);

		UIObject titleBar = attach(new UIRect(), LT, LT)
			.withColor(color(192))
			.withW(this, L, closeButton, L)
			.withH(Coord.PX, 32);

		titleBar.attach(new UIMoveable(), C, C)
			.moves(this);

		this.title = titleBar.attach(new UIText(), C, C)
			.withText("Título")
			.withColor(color(0));

		this.container = attach(new UIRect(), B, B)
			.withH(titleBar, B, this, B)
			.clips();
	}

	UIPopup withTitle(String title)
	{
		this.title.withText(title);
		return this;
	}

	void event(UIEvent e)
	{
		if (e.type == UIEvent.MOUSE_PRESSED && isInside(mouseX, mouseY))
		{
			if (this.parent instanceof UIRoot)
			{
				this.parent.childs.remove(this);
				this.parent.childs.add(this);
			}
		}
	}

	void cb_close()
	{
		dettach();
	}
};
