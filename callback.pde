import java.lang.reflect.Method;

class Callback
{
  Object target;
  String functionName;
  Object userData;

  Callback(Object target, String functionName)
  {
    this.target = target;
    this.functionName = functionName;
    this.userData = null;
  }

  Callback(Object target, String functionName, Object userData)
  {
    this.target = target;
    this.functionName = functionName;
    this.userData = userData;
  }

  boolean exists()
  {
    try
    {
      Method method = target.getClass().getMethod(functionName, null);
    }
    catch (NoSuchMethodException e)
    {
      return false;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      exit();
    }
    return true;
  }

  Object call()
  {
    if (this.userData != null)
      return call(this.userData);

    try
    {
      Method method = target.getClass().getMethod(functionName, null);
      return method.invoke(target);
    }
    catch (java.lang.reflect.InvocationTargetException e)
    {
      e.getCause().printStackTrace();
      exit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      exit();
    }

    return null;
  }

  Object call(Object o)
  {
    try
    {
      Class[] paramTypes = new Class[1];
      paramTypes[0] = o.getClass().forName("java.lang.Object");
      Method method = target.getClass().getMethod(functionName, paramTypes);
      return method.invoke(target, o);
    }
    catch (java.lang.reflect.InvocationTargetException e)
    {
      e.getCause().printStackTrace();
      exit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      exit();
    }

    return null;
  }
};
