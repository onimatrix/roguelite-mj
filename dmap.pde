class DMap
{
	final static int GOAL = 0;
	final static int OBSTACLE = Integer.MAX_VALUE;
	final static int START_VALUE = Integer.MAX_VALUE - 1;

	int w;
	int h;
	int cells[][];

	DMap(int w, int h)
	{
		this.w = w;
		this.h = h;
		this.cells = new int[this.w][this.h];
		for (int y = 0; y < this.h; y++)
			for (int x = 0; x < this.w; x++)
				this.cells[x][y] = DMap.START_VALUE;
	}

	void setGoal(int x, int y)
	{
		this.cells[x][y] = DMap.GOAL;
	}

	void setObstacle(int x, int y)
	{
		this.cells[x][y] = DMap.OBSTACLE;
	}

	int nextDir(int x, int y)
	{
		int current = this.cells[x][y];
		int minValue = current;
		if (x > 0)
		{
			int left = this.cells[x - 1][y];
			if (minValue > left)
				minValue = left;
		}
		if (y > 0)
		{
			int up = this.cells[x][y - 1];
			if (minValue > up)
				minValue = up;
		}
		if (x < this.w - 1)
		{
			int right = this.cells[x + 1][y];
			if (minValue > right)
				minValue = right;
		}
		if (y < this.h - 1)
		{
			int down = this.cells[x][y + 1];
			if (minValue > down)
				minValue = down;
		}

		if (minValue == current)
			return 0;

		IntList dx = new IntList();
		IntList dy = new IntList();

		if (x > 0)
		{
			int left = this.cells[x - 1][y];
			if (minValue == left)
			{
				dx.append(-1);
				dy.append(0);
			}
		}
		if (y > 0)
		{
			int up = this.cells[x][y - 1];
			if (minValue == up)
			{
				dx.append(0);
				dy.append(-1);
			}
		}
		if (x < this.w - 1)
		{
			int right = this.cells[x + 1][y];
			if (minValue == right)
			{
				dx.append(1);
				dy.append(0);
			}
		}
		if (y < this.h - 1)
		{
			int down = this.cells[x][y + 1];
			if (minValue == down)
			{
				dx.append(0);
				dy.append(1);
			}
		}

		int r = randInt(0, dx.size());
		int xx = dx.get(r);
		int yy = dy.get(r);

		if (xx == -1)
			return LEFT;
		if (xx == 1)
			return RIGHT;
		if (yy == -1)
			return UP;
		if (yy == 1)
			return DOWN;

		return 0;
	}

	void reset()
	{
		for (int y = 0; y < this.h; y++)
			for (int x = 0; x < this.w; x++)
			{
				if (this.cells[x][y] == DMap.OBSTACLE)
					continue;
				this.cells[x][y] = DMap.START_VALUE;
			}
	}

	void run()
	{
		while (step());
	}

	boolean step()
	{
		boolean changedSomething = false;

		for (int y = 0; y < this.h; y++)
			for (int x = 0; x < this.w; x++)
			{
				int current = this.cells[x][y];
				if (current == DMap.OBSTACLE)
					continue;

				int minNeighbor = DMap.OBSTACLE;
				if (x > 0)
				{
					int left = this.cells[x - 1][y];
					if (minNeighbor > left)
						minNeighbor = left;
				}
				if (y > 0)
				{
					int up = this.cells[x][y - 1];
					if (minNeighbor > up)
						minNeighbor = up;
				}
				if (x < this.w - 1)
				{
					int right = this.cells[x + 1][y];
					if (minNeighbor > right)
						minNeighbor = right;
				}
				if (y < this.h - 1)
				{
					int down = this.cells[x][y + 1];
					if (minNeighbor > down)
						minNeighbor = down;
				}

				if (current > minNeighbor + 1)
				{
					this.cells[x][y] = minNeighbor + 1;
					changedSomething = true;
				}
			}
		return changedSomething;
	}
};
