class Coord
{
	static final int PX = 0;
	static final int REL = 1;
	static final int DISTANCE = 2;

	int type;
	float value;
	boolean horizontal;

	UIObject a;
	int anchorA;
	UIObject b;
	int anchorB;

	Coord(int type, float value, boolean horizontal)
	{
		this.type = type;
		this.value = value;
		this.horizontal = horizontal;
	}

	Coord(boolean horizontal, UIObject a, int anchorA, UIObject b, int anchorB)
	{
		this.type = Coord.DISTANCE;
		this.a = a;
		this.anchorA = anchorA;
		this.b = b;
		this.anchorB = anchorB;
		this.horizontal = horizontal;
	}

	Coord copy()
	{
		if (this.a != null)
			return new Coord(this.horizontal, this.a, this.anchorA, this.b, this.anchorB);
		return new Coord(this.type, this.value, this.horizontal);
	}

	int getValue(UIObject parent)
	{
		if (this.type == Coord.PX)
			return int(this.value);

		if (this.type == Coord.DISTANCE)
		{
			if (this.horizontal)
			{
				int aX = this.a.x;
				switch (this.anchorA)
				{
					case  T: case  C: case  B: aX += this.a.w / 2; break;
					case  RT: case  R: case  RB: aX += this.a.w; break;
				}

				int bX = this.b.x;
				switch (this.anchorB)
				{
					case  T: case  C: case  B: bX += this.b.w / 2; break;
					case  RT: case  R: case  RB: bX += this.b.w; break;
				}

				return bX - aX;
			}

			int aY = this.a.y;
			switch (this.anchorA)
			{
				case  L: case  C: case  R: aY += this.a.h / 2; break;
				case  LB: case  B: case  RB: aY += this.a.h; break;
			}

			int bY = this.b.y;
			switch (this.anchorB)
			{
				case  L: case  C: case  R: bY += this.b.h / 2; break;
				case  LB: case  B: case  RB: bY += this.b.h; break;
			}

			return bY - aY;
		}

		if (this.horizontal)
			return int((parent.w * this.value) / 100.0f);
		return int((parent.h * this.value) / 100.0f);
	}
};
