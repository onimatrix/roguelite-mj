class FSM
{
	State current;

	FSM(State starting)
	{
		this.current = starting;
	}

	void next(State next)
	{
		this.current = next;
	}

	void update(Enemy owner, float dt)
	{
		this.current.update(owner, this, dt);
	}

	void draw(Enemy owner, float dt)
	{
		this.current.draw(owner, dt);
	}
};

class State
{
	void update(Enemy owner, FSM fsm, float dt)
	{
	}

	void draw(Enemy owner, float dt)
	{
	}
};
