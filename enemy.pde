class EnemyWalkAround extends State
{
	void update(Enemy owner, FSM fsm, float dt)
	{
		PVector movement = new PVector(1.0f, 0.0f);
		movement.rotate(random(TWO_PI));
		owner.vel.add(movement);

		boolean playerIsVisible = owner.isVisible(player.pos, player.getRadius());
		if (playerIsVisible)
			fsm.next(new EnemyPursuit());
	}

	void draw(Enemy owner, float dt)
	{
		float radius = width / enemy.getRadius();

		pushMatrix();
			translate(owner.pos.x, owner.pos.y);
			if (owner.vel.x >= 0.0f)
				scale(-1.0f, 1.0f);

			noStroke();
			fill(8);
			ellipse(0, 0, radius, radius * 0.25f);

			translate(0.0f, -radius * 0.65f);
			scale(radius / 17);
			imageMode(CENTER);
			owner.anims.draw(dt);
			imageMode(CORNER);
		popMatrix();
	}
};

class EnemyPursuit extends State
{
	void update(Enemy owner, FSM fsm, float dt)
	{
		int ex = currentRoom.getTileX(owner.pos.x);
		int ey = currentRoom.getTileY(owner.pos.y);
		int nextDir = currentRoom.dmap.nextDir(ex, ey);

		final float SPEED = 0.3f;
		switch (nextDir)
		{
			case LEFT: 	owner.vel.x -= SPEED; break;
			case RIGHT: owner.vel.x += SPEED; break;
			case UP: 	owner.vel.y -= SPEED; break;
			case DOWN: 	owner.vel.y += SPEED; break;
		}

		boolean playerIsVisible = owner.isVisible(player.pos, player.getRadius());
		if (playerIsVisible == false)
			fsm.next(new EnemySearch());
		else
		{
			float radius = width / owner.getRadius();

			PVector weaponOrigin = new PVector(owner.pos.x, (owner.pos.y - radius * 0.65));
			PVector weaponTarget = player.pos;
			owner.weapon.fire(weaponOrigin, weaponTarget);
		}
	}

	void draw(Enemy owner, float dt)
	{
		float radius = width / owner.getRadius();

		float angle = atan2(player.pos.y - (owner.pos.y - radius * 0.65), player.pos.x - owner.pos.x);
		stroke(255, 0, 0, 192);

		PVector hitPoint = new PVector();
		boolean foundNoObstacle = raycast(int(owner.pos.x), int((owner.pos.y - radius * 0.65)), int(player.pos.x), int(player.pos.y), hitPoint);
		line(owner.pos.x, (owner.pos.y - radius * 0.65), hitPoint.x, hitPoint.y);

		owner.weapon.draw();

		if (foundNoObstacle == false)
		{
			noStroke();
			fill(255, 0, 0);
			ellipse(hitPoint.x, hitPoint.y, 3, 3);
		}

		pushMatrix();
			translate(owner.pos.x, owner.pos.y);
			if (owner.vel.x >= 0.0f)
				scale(-1.0f, 1.0f);

			noStroke();
			fill(8);
			ellipse(0, 0, radius, radius * 0.25f);

			translate(0.0f, -radius * 0.65f);
			scale(radius / 17);
			imageMode(CENTER);
			owner.anims.draw(dt);
			imageMode(CORNER);
		popMatrix();
	}
};

class EnemySearch extends State
{
	float timeSearching;

	EnemySearch()
	{
		this.timeSearching = 0.0f;
	}

	void update(Enemy owner, FSM fsm, float dt)
	{
		int ex = currentRoom.getTileX(owner.pos.x);
		int ey = currentRoom.getTileY(owner.pos.y);
		int nextDir = currentRoom.dmap.nextDir(ex, ey);

		final float SPEED = 0.3f;
		switch (nextDir)
		{
			case LEFT: 	owner.vel.x -= SPEED; break;
			case RIGHT: owner.vel.x += SPEED; break;
			case UP: 	owner.vel.y -= SPEED; break;
			case DOWN: 	owner.vel.y += SPEED; break;
		}

		boolean playerIsVisible = owner.isVisible(player.pos, player.getRadius());
		if (playerIsVisible)
		{
			fsm.next(new EnemyPursuit());
			return;
		}

		this.timeSearching += dt;
		if (this.timeSearching > 3.0f)
			fsm.next(new EnemyWait());
	}

	void draw(Enemy owner, float dt)
	{
		float radius = width / owner.getRadius();

		pushMatrix();
			translate(owner.pos.x, owner.pos.y);
			if (owner.vel.x >= 0.0f)
				scale(-1.0f, 1.0f);

			noStroke();
			fill(8);
			ellipse(0, 0, radius, radius * 0.25f);

			translate(0.0f, -radius * 0.65f);
			scale(radius / 17);
			imageMode(CENTER);
			owner.anims.draw(dt);
			imageMode(CORNER);
		popMatrix();
	}
};

class EnemyWait extends State
{
	float timeWaiting;

	EnemyWait()
	{
		this.timeWaiting = 0.0f;
	}

	void update(Enemy owner, FSM fsm, float dt)
	{
		owner.vel.mult(0.7f);

		boolean playerIsVisible = owner.isVisible(player.pos, player.getRadius());
		if (playerIsVisible)
		{
			fsm.next(new EnemyPursuit());
			return;
		}

		this.timeWaiting += dt;
		if (this.timeWaiting > 5.0f)
			fsm.next(new EnemyWalkAround());
	}

	void draw(Enemy owner, float dt)
	{
		float radius = width / owner.getRadius();

		pushMatrix();
			translate(owner.pos.x, owner.pos.y);
			if (frameCount % 128 < 64)
				scale(-1.0f, 1.0f);

			noStroke();
			fill(8);
			ellipse(0, 0, radius, radius * 0.25f);

			translate(0.0f, -radius * 0.65f);
			scale(radius / 17);
			imageMode(CENTER);
			owner.anims.draw(dt);
			imageMode(CORNER);
		popMatrix();
	}
};

class Enemy extends RoomObject
{
	FSM fsm;

	Weapon weapon;
	AnimationManager anims;

	Enemy(PVector pos)
	{
		super(pos);

		this.fsm = new FSM(new EnemyWalkAround());

		this.weapon = new Weapon(true, 30);

		SpriteSheet ssheet;
		ssheet = new SpriteSheet("enemy_sheet.png", 17, 20, 1, 1);

		this.anims = new AnimationManager();
		this.anims.addFrame("idle", ssheet.frames[0]);
		this.anims.addFrame("walk", ssheet.frames[1]);
		this.anims.addFrame("walk", ssheet.frames[2]);
		this.anims.setActive("idle");
	}

	float getRadius()
	{
		return 50.0f;
	}

	boolean isVisible(PVector pos, float radius)
	{
		PVector middlePos = new PVector(this.pos.x, this.pos.y - radius * 0.65f);

		PVector middle = PVector.sub(pos, middlePos);
		float aRadius = atan(radius / middle.mag());
		PVector ccw = middle.copy();
		ccw.rotate(-aRadius);
		PVector cw = middle.copy();
		cw.rotate(aRadius);

		ccw.add(this.pos);
		cw.add(this.pos);

		PVector hitPoint = new PVector();
		boolean ccwObstacleFree = raycast(int(middlePos.x), int(middlePos.y), int(ccw.x), int(ccw.y), hitPoint);
		if (ccwObstacleFree)
			return true;

		boolean middleObstacleFree = raycast(int(middlePos.x), int(middlePos.y), int(player.pos.x), int(player.pos.y), hitPoint);
		if (middleObstacleFree)
			return true;

		boolean cwObstacleFree = raycast(int(middlePos.x), int(middlePos.y), int(cw.x), int(cw.y), hitPoint);
		if (cwObstacleFree)
			return true;

		return false;
	}

	void update(float dt)
	{
		this.fsm.update(this, dt);

		if (this.vel.mag() > 0.5f)
			this.anims.setActive("walk");
		else
			this.anims.setActive("idle");

		super.update();

		this.weapon.update();
	}

	void draw(float dt)
	{
		this.fsm.draw(this, dt);
	}
};
