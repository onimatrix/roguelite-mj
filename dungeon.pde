class Dungeon
{
	int w;
	int h;
	Room[][] rooms;

	Dungeon(int w, int h, int roomAmountMin, int roomAmountMax)
	{
		this.w = w;
		this.h = h;

		regenerate(roomAmountMin, roomAmountMax);
	}

	Room startRoom()
	{
		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				Room r = this.rooms[xx][yy];
				if (r == null)
					continue;
				if (r instanceof RoomStart)
					return r;
			}
		}
		return null;
	}

	Room endRoom()
	{
		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				Room r = this.rooms[xx][yy];
				if (r == null)
					continue;
				if (r instanceof RoomEnd)
					return r;
			}
		}
		return null;
	}

	void move(int dx, int dy)
	{
		currentRoom = this.rooms[currentRoom.x + dx][currentRoom.y + dy];
	}

	void regenerate(int roomAmountMin, int roomAmountMax)
	{
		do
		{
			generate(roomAmountMin, roomAmountMax);
			if (validate())
				return;
		} while (true);
	}

	boolean validate()
	{
		Room s = null;
		Room e = null;
		int roomsAtBorder = 0;
		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				Room r = this.rooms[xx][yy];
				if (r == null)
					continue;
				if (r instanceof RoomStart)
					s = r;
				else if (r instanceof RoomEnd)
					e = r;
				if (r.x == 0 || r.y == 0 || r.x == this.w - 1 || r.y == this.h - 1)
					roomsAtBorder++;
			}
		}

		if (roomsAtBorder > 4)
			return false;

		if (abs(s.x - e.x) + abs(s.y - e.y) < 4)
			return false;

		if (roomsAround(e) != 1)
			return false;

		return true;
	}

	void generate(int roomAmountMin, int roomAmountMax)
	{
		this.rooms = new Room[this.w][this.h];

		ArrayList<Room> roomsToPlace = new ArrayList<Room>();
		ArrayList<Room> roomsPlaced = new ArrayList<Room>();

		roomsToPlace.add(new RoomStart());
		roomsToPlace.add(new RoomEnd());

		int roomAmount = randInt(roomAmountMin, roomAmountMax - 2);
		for (int r = 0; r < roomAmount; r++)
			roomsToPlace.add(new Room());

		if (roomsToPlace.size() > this.w * this.h)
		{
			println("Too many rooms!");
			exit();
			return;
		}

		Room firstRoom = getRandomRoom(roomsToPlace);
		firstRoom.x = randInt(0, this.w);
		firstRoom.y = randInt(0, this.h);
		this.rooms[firstRoom.x][firstRoom.y] = firstRoom;
		roomsToPlace.remove(firstRoom);
		roomsPlaced.add(firstRoom);

		while (roomsToPlace.size() > 0)
		{
			Room roomToPlace = getRandomRoom(roomsToPlace);
			Room alreadyPlaced = getRandomRoom(roomsPlaced);
			if (placeNextTo(alreadyPlaced, roomToPlace))
			{
				roomsToPlace.remove(roomToPlace);
				roomsPlaced.add(roomToPlace);
			}
		}

		connectRandomly(0.4f);

		for (Room r : roomsPlaced)
			r.openDoors();
	}

	boolean placeNextTo(Room alreadyPlaced, Room roomToPlace)
	{
		IntList dx = new IntList();
		IntList dy = new IntList();

		if (alreadyPlaced.x > 0 && this.rooms[alreadyPlaced.x - 1][alreadyPlaced.y] == null)
		{
			dx.append(-1);
			dy.append(0);
		}
		if (alreadyPlaced.y > 0 && this.rooms[alreadyPlaced.x][alreadyPlaced.y - 1] == null)
		{
			dx.append(0);
			dy.append(-1);
		}
		if (alreadyPlaced.x < this.w - 1 && this.rooms[alreadyPlaced.x + 1][alreadyPlaced.y] == null)
		{
			dx.append(1);
			dy.append(0);
		}
		if (alreadyPlaced.y < this.h - 1 && this.rooms[alreadyPlaced.x][alreadyPlaced.y + 1] == null)
		{
			dx.append(0);
			dy.append(1);
		}

		if (dx.size() == 0)
			return false;

		int choice = int(floor(random(0, dx.size())));
		roomToPlace.x = alreadyPlaced.x + dx.get(choice);
		roomToPlace.y = alreadyPlaced.y + dy.get(choice);
		this.rooms[roomToPlace.x][roomToPlace.y] = roomToPlace;
		connectWithDoors(alreadyPlaced, roomToPlace);
		return true;
	}

	int roomsAround(Room room)
	{
		int count = 0;
		if (room.x > 0 && this.rooms[room.x - 1][room.y] != null)
			count++;
		if (room.y > 0 && this.rooms[room.x][room.y - 1] != null)
			count++;
		if (room.x < this.w - 1 && this.rooms[room.x + 1][room.y] != null)
			count++;
		if (room.y < this.h - 1 && this.rooms[room.x][room.y + 1] != null)
			count++;
		return count;
	}

	void connectWithDoors(Room a, Room b)
	{
		int dx = a.x - b.x;
		int dy = a.y - b.y;

		if (dx == -1)
		{
			a.doorRight = true;
			b.doorLeft = true;
		}
		else if (dx == 1)
		{
			a.doorLeft = true;
			b.doorRight = true;
		}
		else if (dy == -1)
		{
			a.doorBottom = true;
			b.doorTop = true;
		}
		else if (dy == 1)
		{
			a.doorTop = true;
			b.doorBottom = true;
		}
	}

	void connectRandomly(float chance)
	{
		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w - 1; xx++)
			{
				Room a = this.rooms[xx][yy];
				if (a == null || a.doorRight)
					continue;

				Room b = this.rooms[xx + 1][yy];
				if (b == null)
					continue;

				if (random(1.0f) < chance)
					connectWithDoors(a, b);
			}
		}

		for (int yy = 0; yy < this.h - 1; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				Room a = this.rooms[xx][yy];
				if (a == null || a.doorBottom)
					continue;

				Room b = this.rooms[xx][yy + 1];
				if (b == null)
					continue;

				if (random(1.0f) < chance)
					connectWithDoors(a, b);
			}
		}
	}

	Room getRandomRoom(ArrayList<Room> roomList)
	{
		int r = randInt(0, roomList.size());
		return roomList.get(r);
	}

	void drawMap(int x, int y, int w, int h)
	{
		float roomW = w / float(this.w);
		float roomH = h / float(this.h);
		float roomBorderW = roomW * 0.1f;
		float roomBorderH = roomH * 0.1f;
		float roomDoorW = roomW * 0.3f;
		float roomDoorH = roomH * 0.3f;

		noStroke();
		fill(0, 0, 32);
		rect(x, y, w, h);

		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				Room r = this.rooms[xx][yy];
				if (r == null)
					continue;

				if (r instanceof RoomStart)
					fill(0, 255, 0);
				else if (r instanceof RoomEnd)
					fill(255, 0, 0);
				else
					fill(0, 0, 255);

				if (r == currentRoom && frameCount % 100 < 20)
					fill(255);

				rect(	x + xx * roomW + roomBorderW, y + yy * roomH + roomBorderH,
						roomW - roomBorderW * 2, roomH - roomBorderH * 2);

				fill(0, 0, 255);
				if (r.doorLeft)
					rect(	x + xx * roomW, y + yy * roomH + roomDoorH,
							roomBorderW, roomH - roomDoorH * 2);
				if (r.doorRight)
					rect(	x + xx * roomW + roomW - roomBorderW, y + yy * roomH + roomDoorH,
							roomBorderW, roomH - roomDoorH * 2);
				if (r.doorTop)
					rect(	x + xx * roomW + roomDoorW, y + yy * roomH,
							roomW - roomDoorW * 2, roomBorderH);
				if (r.doorBottom)
					rect(	x + xx * roomW + roomDoorW, y + yy * roomH + roomH - roomBorderH,
							roomW - roomDoorW * 2, roomBorderH);
			}
		}
	}
};
