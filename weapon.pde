class Weapon
{
	boolean instant;
	int framesFiring;

	boolean firing;
	int framesSinceFire;
	PVector lastOrigin;
	PVector lastHitPoint;

	Weapon(boolean instant, int framesFiring)
	{
		this.instant = instant;
		this.framesFiring = framesFiring;
	}

	void hit(PVector hitPoint)
	{
	}

	void missed(PVector hitPoint)
	{
	}

	void fire(PVector origin, PVector target)
	{
		if (this.firing)
			return;

		this.framesSinceFire = 0;
		this.firing = true;

		if (this.instant)
		{
			PVector hitPoint = new PVector();
			boolean foundNoObstacle = raycast(int(origin.x), int(origin.y), int(target.x), int(target.y), hitPoint);
			if (foundNoObstacle)
				hit(hitPoint);
			else
				missed(hitPoint);
			this.lastHitPoint = hitPoint.copy();
			this.lastOrigin = origin.copy();
		}
	}

	void update()
	{
		this.framesSinceFire++;
		if (this.framesSinceFire > this.framesFiring)
			this.firing = false;
	}

	void draw()
	{
		if (this.firing)
		{
			float t = this.framesSinceFire / float(this.framesFiring);
			strokeWeight(t * 4.0f);
			stroke(255, 0, 0);
			line(this.lastOrigin.x, this.lastOrigin.y, this.lastHitPoint.x, this.lastHitPoint.y);
		}
		strokeWeight(1.0f);
	}
};
