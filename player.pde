class Player extends RoomObject
{
	AnimationManager anims;
	Weapon weapon;

	Player()
	{
		super(new PVector(width / 2, height / 2));
		this.weapon = new Weapon(true, 4);

		SpriteSheet ssheet;
		ssheet = new SpriteSheet("animation_sheet.png", 17, 20, 1, 1);

		this.anims = new AnimationManager();
		this.anims.addFrame("idle", ssheet.frames[0]);
		this.anims.addFrame("walk", ssheet.frames[1]);
		this.anims.addFrame("walk", ssheet.frames[2]);
		this.anims.setActive("idle");
	}

	float getRadius()
	{
		return 20.0f;
	}

	void update()
	{
		final float SPEED = 1.0f;
		if (kLEFT)
			this.vel.x -= SPEED;
		if (kRIGHT)
			this.vel.x += SPEED;
		if (kUP)
			this.vel.y -= SPEED;
		if (kDOWN)
			this.vel.y += SPEED;

		if (this.vel.mag() > 0.5f)
			this.anims.setActive("walk");
		else
			this.anims.setActive("idle");

		super.update();

		if (frameCount % 8 == 0)
		{
			currentRoom.dmap.reset();
			int px = currentRoom.getTileX(this.pos.x);
			int py = currentRoom.getTileY(this.pos.y);
			currentRoom.dmap.setGoal(px, py);
			currentRoom.dmap.run();
		}

		if (this.pos.x < 0)
		{
			this.pos.x += width;
			dungeon.move(-1, 0);
		}
		if (this.pos.y < 0)
		{
			this.pos.y += height;
			dungeon.move(0, -1);
		}
		if (this.pos.x >= width)
		{
			this.pos.x -= width;
			dungeon.move(1, 0);
		}
		if (this.pos.y >= height)
		{
			this.pos.y -= height;
			dungeon.move(0, 1);
		}

		if (mouseLeft)
		{
			float radius = width / getRadius();
			PVector weaponOrigin = new PVector(this.pos.x, (this.pos.y - radius * 0.65));
			PVector weaponTarget = new PVector(mouseX, mouseY);
			this.weapon.fire(weaponOrigin, weaponTarget);
		}

		this.weapon.update();
	}

	void draw(float dt)
	{
		float radius = width / getRadius();

		float angle = atan2(mouseY - (this.pos.y - radius * 0.65), mouseX - this.pos.x);
		stroke(255, 0, 0, 192);

		PVector hitPoint = new PVector();
		boolean foundNoObstacle = raycast(int(this.pos.x), int((this.pos.y - radius * 0.65)), mouseX, mouseY, hitPoint);
		line(this.pos.x, (this.pos.y - radius * 0.65), hitPoint.x, hitPoint.y);

		this.weapon.draw();

		if (foundNoObstacle == false)
		{
			noStroke();
			fill(255, 0, 0);
			ellipse(hitPoint.x, hitPoint.y, 3, 3);
		}

		pushMatrix();
			translate(this.pos.x, this.pos.y);
			if (this.pos.x < mouseX)
				scale(-1.0f, 1.0f);

			noStroke();
			fill(8);
			ellipse(0, 0, radius, radius * 0.25f);

			translate(0.0f, -radius * 0.65f);
			scale(radius / 17);
			imageMode(CENTER);
			this.anims.draw(dt);
			imageMode(CORNER);
		popMatrix();
	}
};
