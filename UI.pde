class UIEvent
{
	static final int MOUSE_PRESSED = 0;
	static final int MOUSE_RELEASE = 1;

	int type;
	UIObject managedBy;

	UIEvent(int type)
	{
		this.type = type;
		this.managedBy = null;
	}
};

class UIRoot extends UIObject
{
	UIObject active;

	void layout()
	{
		this.x = 0;
		this.y = 0;
		this.w = width;
		this.h = height;

		for (UIObject c : this.childs)
			c.layout();
	}

	UIRoot getRoot()
	{
		return this;
	}
};

static final int LT = 0;
static final int T = 1;
static final int RT = 2;
static final int L = 3;
static final int C = 4;
static final int R = 5;
static final int LB = 6;
static final int B = 7;
static final int RB = 8;

class UIObject
{
	Coord ox;
	Coord oy;
	Coord ow;
	Coord oh;

	int x;
	int y;
	int w;
	int h;

	int align;
	int anchor;

	boolean clips;

	UIObject parent;
	ArrayList<UIObject> childs;

	UIObject()
	{
		this.ox = new Coord(Coord.PX, 0, true);
		this.oy = new Coord(Coord.PX, 0, false);
		this.ow = new Coord(Coord.REL, 100, true);
		this.oh = new Coord(Coord.REL, 100, false);

		this.clips = false;

		this.childs = new ArrayList<UIObject>();
	}

	UIObject withX(int type, int value)
	{
		this.ox = new Coord(type, value, true);
		return this;
	}

	UIObject withY(int type, int value)
	{
		this.oy = new Coord(type, value, false);
		return this;
	}

	UIObject withW(int type, int value)
	{
		this.ow = new Coord(type, value, true);
		return this;
	}

	UIObject withH(int type, int value)
	{
		this.oh = new Coord(type, value, false);
		return this;
	}

	UIObject withX(UIObject a, int anchorA, UIObject b, int anchorB)
	{
		this.ox = new Coord(true, a, anchorA, b, anchorB);
		return this;
	}

	UIObject withY(UIObject a, int anchorA, UIObject b, int anchorB)
	{
		this.oy = new Coord(false, a, anchorA, b, anchorB);
		return this;
	}

	UIObject withW(UIObject a, int anchorA, UIObject b, int anchorB)
	{
		this.ow = new Coord(true, a, anchorA, b, anchorB);
		return this;
	}

	UIObject withH(UIObject a, int anchorA, UIObject b, int anchorB)
	{
		this.oh = new Coord(false, a, anchorA, b, anchorB);
		return this;
	}

	UIObject clips()
	{
		this.clips = true;
		return this;
	}

	<T extends UIObject>
	T attach(T newChild)
	{
		newChild.parent = this;
		newChild.align = LT;
		newChild.anchor = LT;

		this.childs.add(newChild);
		return newChild;
	}

	<T extends UIObject>
	T attach(T newChild, int align, int anchor)
	{
		newChild.parent = this;
		newChild.align = align;
		newChild.anchor = anchor;

		this.childs.add(newChild);
		return newChild;
	}

	void dettach()
	{
		if (this.parent == null)
			return;
		this.parent.childs.remove(this);
		this.parent = null;
	}

	void layout()
	{
		this.x = this.ox.getValue(this.parent);
		this.y = this.oy.getValue(this.parent);
		this.w = this.ow.getValue(this.parent);
		this.h = this.oh.getValue(this.parent);

		this.x += this.parent.x;
		switch (this.align)
		{
			case T: case C: case B: this.x += this.parent.w / 2; break;
			case RT: case R: case RB: this.x += this.parent.w; break;
		}

		this.y += this.parent.y;
		switch (this.align)
		{
			case L: case C: case R: this.y += this.parent.h / 2; break;
			case LB: case B: case RB: this.y += this.parent.h; break;
		}

		switch (this.anchor)
		{
			case  T: case  C: case  B: this.x -= this.w / 2; break;
			case  RT: case  R: case  RB: this.x -= this.w; break;
		}

		switch (this.anchor)
		{
			case  L: case  C: case  R: this.y -= this.h / 2; break;
			case  LB: case  B: case  RB: this.y -= this.h; break;
		}

		for (UIObject c : this.childs)
			c.layout();
	}

	void broadcast(UIEvent e)
	{
		if (e.managedBy != null)
			return;

		event(e);
		ArrayList<UIObject> temp = new ArrayList<UIObject>(this.childs);
		for (UIObject c : temp)
			c.broadcast(e);
	}

	void event(UIEvent e)
	{
	}

	void visit()
	{
		if (this.clips)
			clip(this.x, this.y, this.w, this.h);

		draw();
		for (UIObject c : this.childs)
			c.visit();

		if (this.clips)
			noClip();
	}

	void draw()
	{
	}

	UIRoot getRoot()
	{
		return this.parent.getRoot();
	}

	UIObject getTopObjectOfClass(Class klass, int x, int y)
	{
		if (isInside(x, y) == false)
			return null;

		for (UIObject c : this.childs)
		{
			UIObject found = c.getTopObjectOfClass(klass, x, y);
			if (found != null)
				return found;
		}

		if (getClass() == klass)
			return this;

		return null;
	}

	boolean isInside(int x, int y)
	{
		return !(x < this.x || x >= this.x + this.w || y < this.y || y >= this.y + this.h);
	}
};
