int randInt(int min, int maxExclusive)
{
    return int(floor(random(min, maxExclusive)));
}

// Raycasts from pixel to pixel, returns true if able to reach x1, y1
// hitPoint is set to collision point, even if it's x1, y1
boolean raycast(int x0, int y0, int x1, int y1, PVector hitPoint)
{
	float tileW = width / float(Room.TILES_ON_X);
	float tileH = height / float(Room.TILES_ON_Y);

    int dx = x1 - x0;
    int dy = y1 - y0;
    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float xInc = dx / (float)steps;
    float yInc = dy / (float)steps;

    float xWalk = x0;
    float yWalk = y0;
    for (int i = 0; i <= steps; i++)
    {
    	int tileX = int(floor(xWalk / tileW));
    	int tileY = int(floor(yWalk / tileH));
    	if (tileX < 0 || tileX >= Room.TILES_ON_X ||
    		tileY < 0 || tileY >= Room.TILES_ON_Y ||
    		currentRoom.tiles[tileX][tileY].obstacle)
    	{
    		hitPoint.x = xWalk;
    		hitPoint.y = yWalk;
    		return false;
    	}

        xWalk += xInc;
        yWalk += yInc;
    }
    hitPoint.x = x1;
    hitPoint.y = y1;
    return true;
}
