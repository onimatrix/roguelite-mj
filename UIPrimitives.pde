class UIRect extends UIObject
{
	color col;

	UIRect withColor(color col)
	{
		this.col = col;
		return this;
	}

	void draw()
	{
		noStroke();
		fill(this.col);
		rect(this.x, this.y, this.w, this.h);
	}
};

class UIImage extends UIObject
{
	PImage img;

	UIImage withPath(String imagePath)
	{
		this.img = loadImage(imagePath);
		this.ow = new Coord(Coord.PX, this.img.width, true);
		this.oh = new Coord(Coord.PX, this.img.height, false);
		return this;
	}

	void draw()
	{
		image(this.img, this.x, this.y);
	}
};

class UIText extends UIObject
{
	color col;
	String text;

	UIText()
	{
		this.ow = new Coord(Coord.PX, 0, true);
		this.oh = new Coord(Coord.PX, 0, false);
	}

	UIText withText(String text)
	{
		this.text = text;
		return this;
	}

	UIText withColor(color col)
	{
		this.col = col;
		return this;
	}

	void draw()
	{
		fill(this.col);
		int hAlign = LEFT;
		if (this.anchor == T || this.anchor == C || this.anchor == B)
			hAlign = CENTER;
		else if (this.anchor == RT || this.anchor == R || this.anchor == RB)
			hAlign = RIGHT;
		int vAlign = TOP;
		if (this.anchor == L || this.anchor == C || this.anchor == R)
			vAlign = CENTER;
		else if (this.anchor == LB || this.anchor == B || this.anchor == RB)
			vAlign = BOTTOM;
		textAlign(hAlign, vAlign);
		text(this.text, this.x, this.y);
	}
};

class UIDraggeable extends UIObject
{
	Callback callbackPressed;
	Callback callbackReleased;

	UIDraggeable(Object targetPressed, String functionNamePressed, Object targetReleased, String functionNameReleased)
	{
		this.callbackPressed = new Callback(targetPressed, functionNamePressed);
		this.callbackReleased = new Callback(targetReleased, functionNameReleased);
	}

	void event(UIEvent e)
	{
		if (e.type == UIEvent.MOUSE_PRESSED && isInside(mouseX, mouseY))
		{
			getRoot().active = this;
			e.managedBy = this;
			this.callbackPressed.call();
		}
		if (e.type == UIEvent.MOUSE_RELEASE && getRoot().active == this)
		{
			getRoot().active = null;
			e.managedBy = this;
			this.callbackReleased.call();
		}
	}
};

class UIDragNDropTarget extends UIObject
{
};

class UIDragNDroppeable extends UIObject
{
	Callback callbackDrag;
	Callback callbackDrop;

	UIDragNDroppeable(Object targetDrag, String functionNameDrag, Object targetDrop, String functionNameDrop)
	{
		this.callbackDrag = new Callback(targetDrag, functionNameDrag);
		this.callbackDrop = new Callback(targetDrop, functionNameDrop);
	}

	void event(UIEvent e)
	{
		if (e.type == UIEvent.MOUSE_PRESSED && isInside(mouseX, mouseY))
		{
			getRoot().active = this;
			e.managedBy = this;
			this.callbackDrag.call(this);
		}
		if (e.type == UIEvent.MOUSE_RELEASE && getRoot().active == this)
		{
			getRoot().active = null;
			e.managedBy = this;

			UIObject droppedOn = getRoot().getTopObjectOfClass(UIDragNDropTarget.class, mouseX, mouseY);
			if (droppedOn == null)
			{
				this.callbackDrop.call(this);
				return;
			}

			UIDragNDropTarget dropReceiver = (UIDragNDropTarget)droppedOn;
			this.callbackDrop.call(dropReceiver);
		}
	}
};

class UIClickeable extends UIObject
{
	Callback callback;

	UIClickeable(Object target, String functionName)
	{
		this.callback = new Callback(target, functionName);
	}

	UIClickeable(Object target, String functionName, Object userData)
	{
		this.callback = new Callback(target, functionName, userData);
	}

	void event(UIEvent e)
	{
		if (e.type == UIEvent.MOUSE_PRESSED && isInside(mouseX, mouseY))
		{
			getRoot().active = this;
			e.managedBy = this;
		}

		if (e.type == UIEvent.MOUSE_RELEASE && isInside(mouseX, mouseY) && getRoot().active == this)
		{
			getRoot().active = null;
			e.managedBy = this;
			this.callback.call();
		}
	}
};
